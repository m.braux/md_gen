//! **md_gen is a framework for generating documentation in markdown. It can generate metadata compatible with [Vuepress](https://vuepress.vuejs.org/).**
//! ## Purpose
//! 
//! **md-gen** is intended to generate [Vuepress](https://vuepress.vuejs.org/) compatible (or basic) markdown site.
//! It is usefull for documenting from complex data structures.
//! 
//! By now, every call beginning with `add_*` is generated on its own line in the markdown source. You can mix different formats on the same line using `add_text(text: &str)`.
//! Some exceptions are multi-lines : 
//!  - table headers
//!  - definition
//!  - unordered / ordered lists
//!  - specific code
//!  - ...


pub use crate::md_gen::Markdown;
pub use crate::md_gen::Metadata;


pub mod md_gen;

#[cfg(test)]
mod tests {

    use crate::md_gen::Markdown;
    use crate::md_gen::Metadata;

    #[test]
    fn test_new() {
        let md = Markdown::new("test.md");
        assert_eq!("test.md", md.filename);
    }

    #[test]
    fn test_add_text() {
        let mut md = Markdown::new("test.md");
        md.add_text("This is a test");
        assert_eq!("This is a test", md.lines[0]);

    }

    #[test]
    fn test_add_main_heading() {
        let mut md = Markdown::new("test.md");
        md.add_main_heading("This is a test");
        assert_eq!("# This is a test", md.lines[0]);

    }

    #[test]
    fn test_add_lvl1_heading() {
        let mut md = Markdown::new("test.md");
        md.add_lvl1_heading("This is a test");
        assert_eq!("## This is a test", md.lines[0]);

    }

    #[test]
    fn test_add_lvl2_heading() {
        let mut md = Markdown::new("test.md");
        md.add_lvl2_heading("This is a test");
        assert_eq!("### This is a test", md.lines[0]);

    }

    #[test]
    fn test_add_lvl3_heading() {
        let mut md = Markdown::new("test.md");
        md.add_lvl3_heading("This is a test");
        assert_eq!("#### This is a test", md.lines[0]);

    }

    #[test]
    fn test_add_unordered_list() {
        let mut md = Markdown::new("test.md");
        md.add_unordered_list(vec!["item1", "item2"]);
        assert_eq!("- item1", md.lines[0]);
        assert_eq!("- item2", md.lines[1]);

    }

    #[test]
    fn test_add_blockquoted_unordered_list() {
        let mut md = Markdown::new("test.md");
        md.add_blockquoted_unordered_list(vec!["item1", "item2"]);
        assert_eq!("> - item1", md.lines[0]);
        assert_eq!("> - item2", md.lines[1]);

    }

    #[test]
    fn test_add_ordered_list() {
        let mut md = Markdown::new("test.md");
        md.add_ordered_list(vec!["item1", "item2"]);
        assert_eq!("1. item1", md.lines[0]);
        assert_eq!("2. item2", md.lines[1]);

    }

    #[test]
    fn test_add_simple_code() {
        let mut md = Markdown::new("test.md");
        md.add_simple_code("some code");
        assert_eq!("`some code`", md.lines[0]);

    }

    #[test]
    fn test_add_specific_code() {
        let mut md = Markdown::new("test.md");
        md.add_specific_code("rust", "some code");
        assert_eq!("```rust", md.lines[0]);
        assert_eq!("some code", md.lines[1]);
        assert_eq!("```", md.lines[2]);

    }

    #[test]
    fn test_add_multilines_code() {
        let mut md = Markdown::new("test.md");
        md.add_multilines_code("some code");
        assert_eq!("```", md.lines[0]);
        assert_eq!("some code", md.lines[1]);
        assert_eq!("```", md.lines[2]);

    }

    #[test]
    fn test_add_horizontal_rule() {
        let mut md = Markdown::new("test.md");
        md.add_horizontal_rule();
        assert_eq!("---", md.lines[0]);

    }

    #[test]
    fn test_add_link() {
        let mut md = Markdown::new("test.md");
        md.add_link("link", "text");
        assert_eq!("[text](link)", md.lines[0]);

    }

    #[test]
    fn test_add_image() {
        let mut md = Markdown::new("test.md");
        md.add_image("image", "text");
        assert_eq!("![text](image)", md.lines[0]);

    }

    #[test]
    fn test_add_table_headers() {
        let mut md = Markdown::new("test.md");
        md.add_table_headers(vec!["header1", "header2"]);
        assert_eq!("| header1 | header2 |", md.lines[0]);
        assert_eq!("| --- | --- |", md.lines[1]);

    }

    #[test]
    fn test_add_table_row() {
        let mut md = Markdown::new("test.md");
        md.add_table_row(vec!["cell1", "cell2"]);
        assert_eq!("| cell1 | cell2 |", md.lines[0]);

    }

    #[test]
    fn test_add_blank_line() {
        let mut md = Markdown::new("test.md");
        md.add_blank_line();
        assert_eq!("", md.lines[0]);

    }

    #[test]
    fn test_add_blockquote() {
        let mut md = Markdown::new("test.md");
        md.add_blockquote("quote");
        assert_eq!("> quote", md.lines[0]);

    }

    #[test]
    fn test_add_blockquote_with_multilines() {
        let mut md = Markdown::new("test.md");
        md.add_blockquote("quote line 1\nquote line2\nquote line 3");
        assert_eq!("> quote line 1\nquote line2\nquote line 3", md.lines[0]);

    }

    #[test]
    fn test_add_definition() {
        let mut md = Markdown::new("test.md");
        md.add_definition("word", "a definition");
        assert_eq!("word", md.lines[0]);
        assert_eq!(": a definition", md.lines[1]);

    }

    #[test]
    fn test_add_meta_data() {
        let mut md = Markdown::new("test.md");
        md.add_meta_data(
            Metadata {
                title: "My project doc",
                permalink: "/index.md",
                categories: vec!["myProject", "doc"],
                tags: vec!["tag1", "tag2"].into_iter().collect()
            }
        );
        assert_eq!("---", md.lines[0]);
        assert_eq!("title: My project doc", md.lines[1]);
        assert_eq!("permalink: /index.md", md.lines[2]);
        assert_eq!("categories:", md.lines[3]);
        assert_eq!("  - myProject", md.lines[4]);
        assert_eq!("  - doc", md.lines[5]);
        assert_eq!("tags:", md.lines[6]);
        assert_eq!("  - tag1", md.lines[7]);
        assert_eq!("  - tag2", md.lines[8]);
        assert_eq!("---", md.lines[9]);

    }

    #[test]
    fn test_doc_composition() {
        let mut md = Markdown::new("test.md");
        md.add_meta_data(
            Metadata {
                title: "My project doc",
                permalink: "/index.md",
                categories: vec!["myProject", "doc"],
                tags: vec!["tag1", "tag2"].into_iter().collect()
            }
        );
        md.add_main_heading("Heading");
        md.add_horizontal_rule();
        md.add_blank_line();
        md.add_blockquote("quote");
        md.add_link("/path", "Clic here");

        assert_eq!("---", md.lines[0]);
        assert_eq!("title: My project doc", md.lines[1]);
        assert_eq!("permalink: /index.md", md.lines[2]);
        assert_eq!("categories:", md.lines[3]);
        assert_eq!("  - myProject", md.lines[4]);
        assert_eq!("  - doc", md.lines[5]);
        assert_eq!("tags:", md.lines[6]);
        assert_eq!("  - tag1", md.lines[7]);
        assert_eq!("  - tag2", md.lines[8]);
        assert_eq!("---", md.lines[9]);
        assert_eq!("# Heading", md.lines[10]);
        assert_eq!("---", md.lines[11]);
        assert_eq!("", md.lines[12]);
        assert_eq!("> quote", md.lines[13]);
        assert_eq!("[Clic here](/path)", md.lines[14]);

    }

}