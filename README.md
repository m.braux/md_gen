# md_gen
**md_gen is a framework for generating documentation in markdown. It can generate metadata compatible with [Vuepress](https://vuepress.vuejs.org/).** 

## Purpose

**md-gen** is intended to generate [Vuepress](https://vuepress.vuejs.org/) compatible (or basic) markdown site.
It is usefull for documenting from complex data structures.

By now, every call beginning with `add_*` is generated on its own line in the markdown source. You can mix different formats on the same line using `add_text(text: &str)`.
Some exceptions are multi-lines : 
 - table headers
 - definition
 - unordered / ordered lists
 - specific code
 - ...

## md_gen in action

```rust
use md_gen::{Markdown, Metadata};

fn main() {

    let mut md = Markdown::new("docs/index.md");

    // Create metadata if you generate a vuepress markdown site.
    let metadata = Metadata {
        title: "My project doc",
        permalink: "/index.md",
        categories: vec!["myProject", "doc", "markdown", "vuepress"],
        tags: vec!["tag1", "tag2"].into_iter().collect()
    };

    // Adds metadata to the doc
    md.add_meta_data(metadata);

    // Compose the doc

    // Main Heading
    md.add_main_heading("My project doc"); // # My project doc

    // Adds an horizontal rule
    md.add_horizontal_rule(); // ---

    // Adds a lvl 1 heading (...and so on to lvl 3)
    md.add_lvl1_heading("My lvl 1 title");
    // ## My lvl 1 title

    // Adds a blank line
    md.add_blank_line();  // \n

    // Adds a blocquoted unordered list from a Vec<&str>
    md.add_unordered_list(vec!["item1", "item2"]); 
    // - item1
    // - item2

    // Adds a blank line
    md.add_blank_line();  // \n

    // Adds a blocquoted unordered list from a Vec<&str>
    md.add_blockquoted_unordered_list(vec!["item1", "item2"]); 
    // > - item1
    // > - item2

    // Adds a blank line
    md.add_blank_line();  // \n

    // Adds a blocquoted unordered list from a Vec<&str>
    md.add_ordered_list(vec!["item1", "item2"]); 
    // 1. item1
    // 2. item2

    // Adds a blank line
    md.add_blank_line();  // \n

    // Create a table

    // Table headers
    md.add_table_headers(vec!["Header1", "Header2"]);
    // Table row
    md.add_table_row(vec![
            "Cell content 1",
            "Cell content 2"
        ]);
    }
    // | Header1 | Header2 |
    // | --- | --- |
    // | Cell content 1 | Cell content 2 |

    // Adds a link (internal and external)
    md.add_link("/another_page.md", "Link to another page");
    // [Link to another page](/another_page.md)

    // Adds an image
    md.add_image("assets/image1.png", "Alternative text");
    // ![Alternative text](assets/image1.png)

    // Adds a definition
    md.add_definition("Word", "Definition of this word.");
    // Word
    // : Definition of this word.

    // Adds a block of specific code
    md.add_specific_code("rust", "let foo = MyStruct {\n  bar=\"bar\"\n};");
    // ```rust
    // let foo = MyStruct {
    //   bar="bar"
    // }
    // ```

    // You can also write your own formatted text.
    md.add_text("**[Will Rust win ?](https://www.googlefight.com/rust-vs-rest+of+the+world.php)**");
    // **[Will Rust win ?](https://www.googlefight.com/rust-vs-rest+of+the+world.php)**

    // Try to write the file
    match md.write() {
        Ok(_) => {
            println!("Doc {} has been generated.", md.filename);
        },
        Err(err) => {
            println!("Error while writing {}. {:?}", md.filename, err);
        }
    }

}

```

## About the author and the code

Long time developper, *Rust* beginner. I am open to positive feedbacks about this library and the way it is written or its future features. 
Feel free to contact me.

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
