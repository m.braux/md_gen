use std::collections::VecDeque;

/// Vuepress compatible metadata.
pub struct Metadata<'a> {
    pub title: &'a str,
    pub permalink: &'a str,
    pub categories: Vec<&'a str>,
    pub tags: VecDeque<&'a str>
}

