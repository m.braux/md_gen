extern crate log;
use crate::Metadata;
use log::info;
use std::fs::File;
use std::io::Error;
use std::io::Write;

/// Struct containing the Markdown content and the filename.
pub struct Markdown<'a> {
    pub filename: &'a str,
    pub lines: Vec<String>
}

impl Markdown<'_> {

    /// Create a markdown with the targeted filename.
    pub fn new(filename: &str) -> Markdown {
        Markdown {
            filename: filename,
            lines: vec![]
        }
    }

    /// Create the destination path, adds a final blank line and writes the file.
    pub fn write(&mut self) -> Result<(), Error> {
        let path = std::path::Path::new(self.filename);
        let parent = path.parent();
        if parent.is_some() {
            let prefix = parent.unwrap();
            match std::fs::create_dir_all(prefix) {
                Ok(_) => {
                    info!("Directories of {} successfully updated !", self.filename);
                }, 
                Err(err) => {
                    return Err(err);
                }
            };
        }
        let mut file = File::create(self.filename)?;
        self.add_blank_line();
        let content = self.lines.join("\n");
        file.write_all(content.as_bytes())
    }

    /// Internal function to add a generic line to the Markdown struct.
    fn add_generic_line(&mut self, line: &str) {
        self.lines.push(line.to_string());
    }

    /// Adds a text line (free formatting).
    pub fn add_text(&mut self, text: &str) {
        self.add_generic_line(text);
    }

    ///  Adds a main heading (#).
    pub fn add_main_heading(&mut self, title: &str) {
        self.add_generic_line(&format!("# {}", title));
    }

    /// Adds a level 1 heading (##).
    pub fn add_lvl1_heading(&mut self, title: &str) {
        self.add_generic_line(&format!("## {}", title));
    }

    /// Adds a level 2 heading (###).
    pub fn add_lvl2_heading(&mut self, title: &str) {
        self.add_generic_line(&format!("### {}", title));
    }

    /// Adds a level 3 heading (####).
    pub fn add_lvl3_heading(&mut self, title: &str) {
        self.add_generic_line(&format!("#### {}", title));
    }

    /// Adds an unordered list (- ) from a list.
    pub fn add_unordered_list(&mut self, list: Vec<&str>) {
        for item in list {
            self.add_generic_line(&format!("- {}", item));
        }
    }

    /// Adds a blockquoted unordered list (> - ) from a list.
    pub fn add_blockquoted_unordered_list(&mut self, list: Vec<&str>) {
        for item in list {
            self.add_generic_line(&format!("> - {}", item));
        }
    }

    /// Adds an ordered list (- ) from a list.
    pub fn add_ordered_list(&mut self, list: Vec<&str>) {
        let mut number = 1;
        for item in list {
            self.add_generic_line(&format!("{}. {}", number, item));
            number += 1;
        }
    }

    /// Adds a simple code (`code`).
    pub fn add_simple_code(&mut self, code: &str) {
        self.add_generic_line(&format!("`{}`", code));
    }

    /// Adds a multiline code with specific language.
    pub fn add_specific_code(&mut self, language:&str, code: &str) {
        self.add_generic_line(&format!("```{}", language));
        self.add_generic_line(&format!("{}", code));
        self.add_generic_line("```");
    }

    /// Adds a multilines code.
    pub fn add_multilines_code(&mut self, code: &str) {
        self.add_generic_line("```");
        self.add_generic_line(&format!("{}", code));
        self.add_generic_line("```");
    }

    /// Adds an horizontal rule (---).
    pub fn add_horizontal_rule(&mut self) {
        self.add_generic_line(&format!("---"));
    }

    /// Adds a link with a title.
    pub fn add_link(&mut self, link:&str, title: &str) {
        self.add_generic_line(&format!("[{}]({})", title, link));
    }

    /// Adds an image with an alt text.
    pub fn add_image(&mut self, image:&str, alt_text: &str) {
        self.add_generic_line(&format!("![{}]({})", alt_text, image));
    }

    /// Adds table headers (headers + --- cells) from a vector of headers.
    pub fn add_table_headers(&mut self, headers: Vec<&str>) {
        let mut seps: Vec<&str>= vec![];
        for _header in &headers {
            seps.push("---");
        }
        self.add_generic_line(&format!("| {} |", headers.join(" | ")));
        self.add_generic_line(&format!("| {} |", seps.join(" | ")));
    }

    /// Adds a table row from a vector of cells.
    pub fn add_table_row(&mut self, cells: Vec<&str>) {
        self.add_generic_line(&format!("| {} |", cells.join(" | ")));
    }

    /// Adds a blank line.
    pub fn add_blank_line(&mut self) {
        self.add_generic_line("");
    }

    /// Adds a blockquote. The blockquote can be multiline while no double `\n` is met.
    pub fn add_blockquote(&mut self, blockquote: &str) {
        self.add_generic_line(&format!("> {}", blockquote));
    }

    /// Adds a definition.
    pub fn add_definition(&mut self, terme: &str, definition: &str) {
        self.add_generic_line(&format!("{}", terme));
        self.add_generic_line(&format!(": {}", definition));
    }

    /// Adds predefined Vuepress compatible metadatas. Must be call before composing the doc.
    pub fn add_meta_data(&mut self, meta: Metadata) {
        self.add_horizontal_rule();
        self.add_text(&format!("title: {}", meta.title));
        self.add_text(&format!("permalink: {}", meta.permalink));
        self.add_text("categories:");
        for categorie in meta.categories {
            self.add_generic_line(&format!("  - {}", categorie));
        }
        self.add_text("tags:");
        for tag in meta.tags {
            self.add_generic_line(&format!("  - {}", tag));
        }
        self.add_horizontal_rule();
    }
}